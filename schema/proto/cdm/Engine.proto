syntax = "proto3";
package cdm;
option java_package = "com.kitware.physiology.cdm";
option optimize_for = SPEED;

import "cdm/Enums.proto";
import "cdm/Actions.proto";
import "cdm/Patient.proto";
import "cdm/PatientActions.proto";
import "cdm/PatientConditions.proto";
import "cdm/EnvironmentActions.proto";
import "cdm/EnvironmentConditions.proto";
import "cdm/AnesthesiaMachineActions.proto";
import "cdm/InhalerActions.proto";
import "cdm/Properties.proto";

message DecimalFormatData
{
enum eType
{
  SystemFormatting  = 0;/*<<@brief Not provided, let the system do what it wants */
  DefaultFloat      = 1;
  FixedMantissa     = 2;
  SignificantDigits = 3;
}

  eType                                                Type                      = 1;
  uint32                                               Precision                 = 2;
}

message DataRequestData
{
  enum eCategory
  {
    Patient            = 0;
    Physiology         = 1;
    Environment        = 2;
    GasCompartment     = 3;
    LiquidCompartment  = 4;
    ThermalCompartment = 5;
    TissueCompartment  = 6;
    Substance          = 7;
    AnesthesiaMachine  = 8;
    ECG                = 9;
    Inhaler            = 10;
  }
  
  DecimalFormatData                                    DecimalFormat             = 1;/**<<@brief If writing to a file, specify the decimal formatting */
  eCategory                                            Category                  = 2;/**<<@brief A category assocated with the Name */
  string                                               CompartmentName           = 3;/**<<@brief The compartment name the property could be associated with */
  string                                               SubstanceName             = 4;/**<<@brief The substance name the property could be associated with */
  string                                               PropertyName              = 5;/**<<@brief The name of the property requested */
  string                                               Unit                      = 6;/**<<@brief If writing to a file, the unit the data will be in. */
}
message DataRequestManagerData
{
  string                                               ResultsFilename           = 1;
  double                                               SamplesPerSecond          = 2;
  DecimalFormatData                                    DefaultDecimalFormatting  = 3;
  DecimalFormatData                                    OverrideDecimalFormatting = 4;
  repeated DataRequestData                             DataRequest               = 5;
}

message AnyConditionData
{
  oneof Condition
  {
    AnyPatientConditionData                            PatientCondition          = 1;
    AnyEnvironmentConditionData                        EnvironmentCondition      = 2;
  }
}
message ConditionListData
{
  repeated AnyConditionData                            AnyCondition              = 1;
}

message AnyActionData
{
  oneof Action
  {
    AdvanceTimeData                                     AdvanceTime               = 1;
    SerializeStateData                                  Serialize                 = 2;
    AnyPatientActionData                                PatientAction             = 3;
    AnyEnvironmentActionData                            EnvironmentAction         = 4;
    AnyAnesthesiaMachineActionData                      AnesthesiaMachineAction   = 5;
    AnyInhalerActionData                                InhalerAction             = 6;
  }
}
message ActionListData
{
  repeated AnyActionData                                AnyAction                 = 1;
}

message EngineInitializationData
{
  oneof StartType
  {
    string                                              EngineStateFile           = 1;
    PatientConfigurationData                            PatientConfiguration      = 2;
  }
  string                                                LogFile                   = 3;
}
message PatientConfigurationData
{
  oneof PatientType
  {
    PatientData                                         Patient                   = 1;
    string                                              PatientFile               = 2;
  }
  ConditionListData                                     Conditions                = 3;
}

message AutoSerializationData
{
  string                                                Directory                      = 1;/**<< @brief The location of the state file being written to. */
  string                                                Filename                       = 2;/**<< @brief The file name of the state file being written to. */
  eSwitch                                               AfterActions                   = 3;/**<< @brief Save state after an action is added to the action manager, then again after it is processed. (These states are always time stamped.) */
  ScalarTimeData                                        Period                         = 4;/**<< @brief The frequency at which to write a file, 1s would write a file every second. 0 will not save out at all (if you wanted after actions only.) */    
  eSwitch                                               PeriodTimeStamps               = 5;/**<< @brief Put a time stamp at the end of the period state filename, Off will overwrite the same individual file, On will create new files every period, named with the simulation time. */
  eSwitch                                               ReloadState                    = 6;/**<< @brief Load the data back into the engine, this is primarily used for testing */
}

/** @brief Static timed stabilization lengths */
message TimedStabilizationData
{
  eSwitch                                               TrackingStabilization          = 1;/**<< @brief Track data requests to the output file (if provided) during stabilization. */
  ScalarTimeData                                        RestingStabilizationTime       = 2;/**<< @brief */
  ScalarTimeData                                        FeedbackStabilizationTime      = 3;/**<< @brief */
  map<string,ScalarTimeData>                            ConditionStabilization         = 4;/**<< @brief */
}

/** @brief A properties target percent difference allowed */
message DynamicStabilizationPropertyConvergenceData
{
  DataRequestData                                       DataRequest                     = 1;/**<< @brief */
  double                                                PercentDifference               = 2;/**<< @brief */
}
/** @brief A properties convergence timing */
message DynamicStabilizationEngineConvergenceData
{
  ScalarTimeData                                        ConvergenceTime                 = 1;/**<< @brief */
  ScalarTimeData                                        MinimumReactionTime             = 2;/**<< @brief */
  ScalarTimeData                                        MaximumAllowedStabilizationTime = 3;/**<< @brief */
  repeated DynamicStabilizationPropertyConvergenceData  PropertyConvergence             = 4;/**<< @brief */
}
/** @brief Dynamic stabilization parameters */
message DynamicStabilizationData
{
  eSwitch                                               TrackingStabilization           = 1;/**<< @brief Track data requests to the output file (if provided) during stabilization. */
  DynamicStabilizationEngineConvergenceData             RestingConvergence              = 2;/**<< @brief */
  DynamicStabilizationEngineConvergenceData             FeedbackConvergence             = 3;/**<< @brief */
  map<string,DynamicStabilizationEngineConvergenceData> ConditionConvergence            = 4;/**<< @brief */
}

message LogMessagesData
{
  repeated string                                       DebugMessages                   = 1;
  repeated string                                       InfogMessages                   = 2;
  repeated string                                       WarningMessages                 = 3;
  repeated string                                       ErrorMessages                   = 4;
  repeated string                                       FatalMessages                   = 5;
}
